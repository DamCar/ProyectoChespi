# Importado de librerias necesarias para el funcionamiento de los metodos

import math
import socket
from sys import argv

#Metodo que cambia un valor de tipo string a binario
def strbin(a):
    res=0;#variable donde se almacena el resultado
    #Ciclo for donde se pasa por todo el arreglo de chars (variable de tipo string)
    for i in a:
        res<<=7 #Corrimiento de 7 espacios a la izquierda
        res^=ord(i) #Combinacion de la secuencia de bits
    return res #retorno del resultado


# Metodo que determina la distancia entre una secuencia a y una secuencia b
# Basicamente este metodo encuentra las diferencias entre ambas secuencias
def hamming(a,b):
    a^=b
    b=0
    while a>0:
        c=int(math.log(a,10)/math.log(2,10)) #Obtencion de la posicion del bit mas significativo
        if (a&1<<c)>>c==1:
            a&=~(1<<c)
            b+=1
    return b


# Metodo para aplicar  FEC(Forward Error Correction)
# Este metodo utiliza la siguiente secuencia
# 00 -> 00000 (0)
# 01 -> 11100 (28)
# 10 -> 01111 (19)
# 11 -> 10011 (15)
# Este metodo analiza las secuencias de bits  de par en par, de izquierda a derecha
# intercambiando la secuencia original por los diccionarios equivalentes
# en este metodo se opto por no utilizar diccionarios pues no eran necesarios
# mas bien implicaban un gasto de recursos poco fortuito.

# a -> hace referencia a la secuencia de bits a convertir
# l -> hace referencia al tamaño en bits de la secuencia  "a"
def c5(a,l):
    c=l-2
    #l=(l/2)*5
    x=0
    while c>=0:
        x<<=5
        op=(a & 3 << c) >> c
        if op==1:
            x^=28
        elif op==2:
            x^=19
        elif op==3:
            x^=15
        c-=2
    return x

#Metodo de Para verificar y corregir errores
def c2(a,l):
    c=l-5
    x=0
    while c>=0:
        x<<=2
        op=(a & 31 << c) >> c
        if op==28:
            x^=1
        elif op==19:
            x^=2
        elif op==15:
            x^=3
        elif op!=0:
            # en esta parte se utiliza distancia hamming comparando el numero de errores
            # en la secuencia  comparandolos de 5 en 5 bits, luego se comparan con la operacion que devuelva menos errores
            # para despues determinar cual es la secuencia real de bits a utilizar
            index=0
            d=[]
            d.push(hamming(op,0))
            d.push(hamming(op,28))
            d.push(hamming(op,19))
            d.push(hamming(op,15))
            min=d[0]
            i=1
            while i<4:
                if min>d[i]:
                    min=d[i]
                    index=i
                i+=1
            x^=index
        c-=5
    return x


#------------------------------------------------------------------------------
# Seccion de pruebas
#vari="a"
#n=len(vari)*8
#print n
#vari=strbin(vari)
#print vari
#vari=c5(vari,n)
#print vari
#n=n/2*5
#vari=c2(vari,n)
#print vari
#----------------------------------------------------------
program, localr, rol = argv
if localr=='l':
    host = socket.gethostname() # for ethernet test: '192.168.0.2'# for local test: socket.gethostname()
elif localr=='r':
    host = '192.168.0.2'
port, s = 1234, socket.socket()
if rol == 't':
    message = raw_input("->")
    n=len(message)*8
    message=c5(strbin(message),n)
    print "type this # ",n/2*5
    s.connect((host, port))
    s.send(str(message))
elif rol == 'r':
    s.bind((host,port))
    s.listen(1)
    c, addr = s.accept()
    data = c.recv(4096)
    print "Connection from: "+ str(addr) +"\nData from connected user: "+str(data)
    tam=raw_input("type data: ")
    data=c2(int(data),int(tam))
    print data
else:
    print "Intenta de nuevo..."
s.close()
