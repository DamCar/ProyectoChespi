# Librerias necesarias para el funcionamiento de la app

import math
import socket
from sys import argv

#Obtiene el polinomio
def poly(n,debug):
    res=0
    while n>0:
        res+=2**n
        n-=1
    if debug==1:
        print "Polinomio: ",bin(res)
    return res

#Metodo que cambia un valor de tipo string a binario
def strbin(a):
    res=0;#variable donde se almacena el resultado
        #Ciclo for donde se pasa por todo el arreglo de chars (variable de tipo string)
    for i in a:
        res<<=7 #Corrimiento de 7 espacios a la izquierda
        res^=ord(i) #Combinacion de la secuencia de bits
    return res #retorno del resultado

#Retorna la posicion  del bit mas significativo
def sig(a):
    return int(math.log(a,10)/math.log(2,10))

# Metodo para efectuar la division
def divition(a,p,debug):
    res=a
    pls=sig(p)
    a<<=pls
    while sig(a)>=pls:
        if debug==1:
            print""
            print bin(a)
            print bin(p<<(sig(a)-pls))
            print "---------------------------------"
        a^=p<<(sig(a)-pls)
        if debug==1:
            print bin(a)
            print""
    res<<=pls
    res^=a
    if debug==1:
        print "Resultado: ",res
    return res

def noise(a,x,debug):
    if debug==1:
        print "Generando ruido..."
        print "Original: ",bin(a)
        print "Con Ruido: ",bin(a|x)
    a|=x
    return a

#Metodo para efectuar el chequeo
def check(a,p,debug):
    print"Revisando secuencia..."
    res=a
    pls=sig(p)
    while (a!=0 and sig(a)>=pls):
        if debug==1:
            print""
            print bin(a)
            print bin(p<<(sig(a)-pls))
            print "---------------------------------"
        a^=p<<(sig(a)-pls)
        if debug==1:
            print bin(a)
            print""
    if debug==1:
        print "Resultado de Revision: ",bin(a)
    if a==0:
        return 1
    else:
        return 0

#Obtencion de variables
#hstloc
#l -> local
#r -> remoto
hstloc=argv[1]
#rol
#rx->receptor
#tx->transmisor
rol=argv[2]

if hstloc=="r":
    hstip=raw_input("Host IP: ")
    #Descomentar estas linea para elegir el polinomio
    order=raw_input("Enter the poly: ")
    debug=int(raw_input("Activate debug? (1->yes , other->no): "))
    #pol=poly(int(order))
if hstloc=="l":
    hstip=socket.gethostname()
    #Descomentar estas linea para elegir el polinomio
    order=raw_input("Enter the poly: ")
    debug=int(raw_input("Activate debug? (1->yes , other->no): "))

port, s = 1234, socket.socket()


if rol=="tx":
    s.connect((hstip, port))
    msg=raw_input(">")
    print "Original: ",msg
    msg=strbin(msg)
    print "strbin(msg): ",bin(msg)
    msg=divition(msg,int(order),debug)
    op=int(raw_input("Insertar ruido? (1->yes,other->no) :"))
    if op==1:
        ns=int(raw_input("Ingrese un numero base para el ruido: "))
        msg=noise(msg,ns,debug)
    print "enviando informacion..."
    s.send(str(msg))
if rol=="rx":
    print "waiting for data..."
    s.bind((hstip,port))
    s.listen(1)
    c,addr=s.accept()
    data=int(c.recv(4069))
    print data
    print bin(data)
    if check(data,int(order),debug)==1:
        print "Informacion Recibida sin errores!!"
    else:
        print "Informacion Recibida con errores!!"
s.close()
