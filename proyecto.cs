using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            string x = "a";
            int n = x.Length * 8;
            int ruido=c5(strbin(x), ref n);
            Console.WriteLine(ruido);
            ruido |= 66;
            Console.WriteLine(c2(ruido, n));
        }

        static int c2(int a,int l)
        {
            int c=l-5;
            int x = 0;
            while (c >= 0)
            {
                x <<= 2;
                switch ((a & 31 << c) >> c)
                {
                    case 0: break;
                    case 28: x ^= 1; break;
                    case 19: x ^= 2; break;
                    case 15: x ^= 3; break;
                    default:
                        int op = (a & 31 << c) >> c,min, index=0;
                        int[] d = new int[4] { hamming(op, 0), hamming(op, 28), hamming(op, 19), hamming(op, 15) };
                        min = d[0];

                        for (int i = 1; i < d.Length; i++)
                            if (min > d[i])
                            {
                                min = d[i];
                                index = i;
                            }
                        x ^= index;
                        break;
                }
                c -= 5;
            }
            return x;
        }


        static int c5(int a, ref int l)
        {
            int c=l-2;
            l= (l/2) * 5;
            int x=0;
            while (c>=0)
            {
                //c = (int)(Math.Log10(a) / Math.Log10(2)) - 1;
                x <<= 5;
                switch ((a & 3 << c) >> c)
                {
                    case 1:x^= 28;break;
                    case 2:x^=19; break;
                    case 3:x ^= 15; break;
                }
                c -= 2;
                //a&=~(3 << c);
            }
            return x;
        }
        static int strbin(string a)
        {
            int x=0;
            for (int i = 0; i < a.Length; i++)
            {
                x <<= 7;
                x ^= a[i];
            }
            return x;
        }

        static int hamming(int a,int b)
        {
            a^= b; //Xor
            b = 0; //Contador
            int c;//Posicion
            while (a>0)
            {
                c = (int)(Math.Log10(a) / Math.Log10(2));
                if ((a&1<<c)>>c==1)
                {
                    a &= ~(1 << c);
                    b++;
                }
            }
            return b;
        }
    }


}
