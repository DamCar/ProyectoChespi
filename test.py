import math
import socket
from sys import argv
def divisiones(bit, bits):	
	bit, gen, la = bit<<(bits - 1), (1<<(bits - 1)) + 1, int(math.log(bit,2)) + 1 - bits
	res= 1<<(int(math.log(bit,2)) + 1)
	while res >= gen:
		if bit == 0:
			return res
		la = int(math.log(bit,2)) + 1 - bits
		res=bit%(1<<la)
		bit=(((bit>>la)^gen)<<la)+res				
	return res
def Main():
	program, localr, rol, bits = argv
	if localr=='l':
		host = socket.gethostname() # for ethernet test: '192.168.0.2'# for local test: socket.gethostname()
	elif localr=='r':
		host = '192.168.0.2'				
	port, s = 9985, socket.socket()
	if rol == 't':				
		bit, message = 0, raw_input("->")
		s.connect((host, port))
		for i in message:		
			bit=(ord(i)+bit)<<7				
		bit = (bit<<(int(bits)-1)) + (divisiones(bit>>7, int(bits)))
		s.send(str(bit))
	elif rol == 'r':	
		s.bind((host,port))
		s.listen(1)
		c, addr = s.accept()
		data = c.recv(4096)		
		print "Connection from: "+ str(addr) +"\nData from connected user: "+str(data)		
		if divisiones(int(data), int(bits)):
			print "Hay error"
		else:
			print "No hay error"
	else:
		print "Intenta de nuevo..."
	s.close()		
if __name__ == '__main__':
	Main()	
